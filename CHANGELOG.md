# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.2](https://gitlab.com/svartkonst/traits/compare/v0.0.1...v0.0.2) (2020-06-30)

### 0.0.1 (2020-06-30)
