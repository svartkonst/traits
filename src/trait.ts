/* eslint-disable @typescript-eslint/no-explicit-any */
type ImplCallback = (...args: any[]) => any;

export class Trait {
  private __trait: symbol;
  private __impls: Map<Function, ImplCallback>;

  get trait(): symbol {
    return this.__trait;
  }

  get impls(): Map<Function, ImplCallback> {
    return this.__impls;
  }

  constructor(public name: string) {
    this.__trait = Symbol(name);
    this.__impls = new Map();
  }

  public invoke<T = any, U = any>() {
    return (instance: any, ...args: T[]): U => {
      if (instance[this.trait]) {
        /* eslint-disable-next-line */
        return instance[this.trait].apply(instance, args);
      }

      const regImpl = this.impls.get(instance.constructor);

      if (regImpl) {
        return regImpl.apply(instance, [instance, ...args]);
      }

      throw new Error(`${instance} does not implement ${this.name}`);
    }
  }

  public impl(ctor: Function, fn: ImplCallback): void {
    this.__impls.set(ctor, fn);
  }

  public implementedFor(ctor: Function): boolean {
    return !!ctor.prototype[this.trait] || this.impls.has(ctor);
  }
}
